## Overview
This project is designed to demonstrate a robust web application architecture using AWS services. It consists of a front-end and a back-end service, both running on a single EC2 instance. These services are containerized using Docker, ensuring easy deployment and scalability. The back-end container interacts with an RDS endpoint for database operations. An Application Load Balancer (ALB) is utilized to distribute incoming traffic across two availability zones, enhancing the application's availability and fault tolerance.

## Subnets
The architecture includes 1 public and 1 private subnet in each availability zone. The EC2 instances are located in the public subnets, allowing them to be accessible from the internet, while the RDS database and its replica are securely placed in private subnets.

## Deployment
The deployment utilizes Docker containers for both the front-end and back-end services, running on an EC2 instance. This setup is managed through AWS services, ensuring a scalable and secure environment. The use of an ALB and multiple subnets across availability zones guarantees high availability and fault tolerance.

## Security
- The EC2 instances are placed in public subnets with security groups allowing only necessary traffic.
- The RDS database is placed in a private subnet, restricting direct access from the internet and ensuring data security.
- Communication between the EC2 instances and the RDS endpoint is secured within the VPC.
- The ALB is protected by a Web Application Firewall (WAF) that provides additional security against common web exploits and attacks.

## Prerequisites
- AWS Account
- Docker
- Knowledge of AWS VPC, EC2, RDS, ALB, and WAF

<p align="center">
  <img src="diagram.png">
</p>

## Setup and Deployment
1. **VPC Configuration**: Sets up a VPC with 2 availability zones, each containing 1 public and 1 private subnet.
2. **EC2 Setup**: Launches an EC2 instance in the public subnet and install Docker for running the front-end and back-end containers.
3. **RDS Setup**: Creates an RDS PostgreSQL instance in the private subnet of `eu-central-1a` AZ and a replica in `eu-central-1b`'s private subnet for high availability.
4. **ALB Configuration**: Sets up an Application Load Balancer to distribute traffic between the EC2 instances across the two availability zones. Attach a Web Application Firewall (WAF) with basic rules for DDoS protection and IP blacklisting.
5. **Security Groups**: Configures security groups for EC2 and RDS to manage access control.

## Planned Implementations for the Future

6. **CloudFront**: Set the origin of the CloudFront distribution to the ALB.
7. **Route 53 (DNS)**:
   - Register a domain with a public hosted zone.
   - Create a record in Route 53 pointing to the CloudFront distribution.
8. **AWS Certificate Manager**:
   - Generate an SSL/TLS certificate.
   - Validate the certificate with a CNAME record.
   - Assign the certificate to the CloudFront distribution.

## CI/CD and Infrastructure as Code

### GitLab CI
This project uses GitLab CI for continuous integration and deployment. A `.gitlab-ci.yml` file defines the pipeline stages, including build, test, and deployment processes. This ensures that every change pushed to the repository is automatically built and tested, and if the tests pass, the changes are deployed to the production environment.

### Docker Compose
Docker Compose is used to define and run multi-container Docker applications. With a `docker-compose.yml` file, the front-end and back-end services, along with any other necessary services, are defined. This allows for easy local development and testing, ensuring that the application runs in an environment similar to production.

### Terraform
Terraform is utilized for provisioning and managing the AWS infrastructure as code. It allows for the definition of infrastructure through configuration files that can be versioned and reused. This project's infrastructure, including the VPC, EC2 instances, RDS database, ALB, and security groups, is defined in Terraform files. This approach enables consistent and reproducible infrastructure deployments.

## Conclusion
This project outlines a scalable and secure web application architecture using AWS services, Docker, GitLab CI, Docker Compose, and Terraform. By leveraging these tools and services, the application ensures high availability, fault tolerance, efficient load balancing, and protection against common web exploits and attacks. The use of infrastructure as code and continuous integration and deployment practices further enhances the project's maintainability and scalability.

## Contributors
- Aleksandre Mikashavidze (DevOps)
- Gvantsa Euashvili (Backend)
- Mariam Khutuashvili (Frontend)
- Aleksandre Gelashvili (Mobile - IOS (Separate Environment))
- Revaz Mskhvilidze (Cybersecurity)
- Saba Chegelidze (Information Security)
