# EC2 Instance Security Group
resource "aws_security_group" "dreamcatchers-ec2-sg" {
  name        = "dreamcatchers-ec2-sg"
  description = "Allow traffic from VPC"
  vpc_id      = aws_vpc.dreamcatchers-vpc.id
  depends_on = [
    aws_vpc.dreamcatchers-vpc
  ]

  ingress {
    from_port = "0"
    to_port   = "0"
    protocol  = "-1"
  }
  ingress {
    from_port   = "80"
    to_port     = "80"
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    from_port   = "22"
    to_port     = "22"
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

# ingress {
#     from_port       = 5432
#     to_port         = 5432
#     protocol        = "tcp"
#     security_groups = [aws_security_group.dreamcatchers-db-sg.id]
# }

  egress {
    from_port   = "0"
    to_port     = "0"
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "dreamcatchers-ec2-sg"
  }
}

# Application Load balancer security group
resource "aws_security_group" "dreamcatchers-alb-sg" {
  name        = "dreamcatchers-alb-sg"
  description = "load balancer security group"
  vpc_id      = aws_vpc.dreamcatchers-vpc.id
  depends_on = [
    aws_vpc.dreamcatchers-vpc
  ]

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = -1
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
    ingress {
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "dreamcatchers-alb-sg"
  }
}

# RDS Database Security gruop
resource "aws_security_group" "dreamcatchers-db-sg" {
  name        = "dreamcatchers-db-sg"
  description = "allow traffic from EC2 instances"
  vpc_id      = aws_vpc.dreamcatchers-vpc.id

  ingress {
    from_port       = 5432
    to_port         = 5432
    protocol        = "tcp"
    security_groups = [aws_security_group.dreamcatchers-ec2-sg.id]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}