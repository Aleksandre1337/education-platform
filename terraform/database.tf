# RDS PostgreSQL database

resource "aws_db_subnet_group" "dreamcatchers-db-subnet-group" {
    name       = "dreamcatchers-db-subnet-group"
    #vpc_id     = aws_vpc.dreamcatchers-vpc.id
    subnet_ids = [aws_subnet.dreamcatchers-pvt-sub-1.id, aws_subnet.dreamcatchers-pvt-sub-2.id]

    tags = {
        Name = "dreamcatchers-db-subnet-group"
    }
}

resource "aws_db_parameter_group" "dreamcatchers-db-parameter-group" {
    name        = "dreamcatchers-db-parameter-group"
    family      = "postgres16"
    description = "Parameter group for dreamcatchers-db-1"

    tags = {
        Name = "dreamcatchers-db-parameter-group"
    }
}

resource "aws_db_instance" "dreamcatchers-db-1" {
  identifier             = var.postgres_identifier
  instance_class         = var.postgres_instance_class
  allocated_storage      = var.postgres_allocated_storage
  engine                 = var.postgres_engine
  engine_version         = var.postgres_engine_version
  username               = var.postgres_db_username
  password               = var.postgres_db_password
  db_subnet_group_name   = aws_db_subnet_group.dreamcatchers-db-subnet-group.name
  vpc_security_group_ids = [aws_security_group.dreamcatchers-db-sg.id]
  parameter_group_name   = aws_db_parameter_group.dreamcatchers-db-parameter-group.name
  publicly_accessible    = false
  skip_final_snapshot    = true
  performance_insights_enabled = false
  deletion_protection = false
  multi_az = true
}