# Provider Variables

variable "aws_region" {
  description = "The AWS region to deploy resources"
  type        = string
  default     = "us-east-1"
}

# Access keys for terraform user
variable "access_key" {
  description = "aws access_key for terrauser"
  type        = string
  default     = "secret1"
}

variable "secret_access_key" {
  description = "aws secret_access_key for terrauser"
  type        = string
  default     = "secret2"
}

variable "session_token" {
  description = "AWS session token"
  type        = string
  default     = "secret3"
}

# RDS PostgreSQL Variables

variable "postgres_db_name" {
    description = "Set database name for PostgreSQL"
    type        = string
    default     = "dreamcatchers-database"
}

variable "postgres_db_username" {
  description = "The username for the RDS instance"
  type        = string
  default     = "dreamcatchers"
}

variable "postgres_db_password" {
    description = "Set password for PostgreSQL database"
    type        = string
    default     = "tbcgang123"
}

variable "postgres_db_instance_name" {
    description = "Unique name for PostgreSQL DB instance"
    type        = string
    default     = "dreamcatchers-instance"
}

variable "postgres_port" {
    description = "Default port for PostgreSQL"
    type        = number
    default     = 5432
}
variable "postgres_identifier" {
  description = "Set identifier for PostgreSQL"
  type        = string
  default     = "dreamcatchers-db-1"
}

variable "postgres_instance_class" {
  description = "Instance class for PostgreSQL"
  type        = string
  default     = "db.t3.micro"
}

variable "postgres_allocated_storage" {
  description = "Allocated storage for PostgreSQL"
  type        = number
  default     = 50
}

variable "postgres_engine" {
  description = "Engine for PostgreSQL"
  type        = string
  default     = "postgres"
}

variable "postgres_engine_version" {
  description = "Engine version for PostgreSQL"
  type        = string
  default     = "16.1"
}


# Variables for EC2

variable "ec2_ami" {
  description = "Ami For Ec2"
  type = string
  default = "ami-0a0e5d9c7acc336f1"
}

variable "ec2_instance_type" {
  description = "Instance type for EC2"
  type        = string
  default     = "t2.micro"
}

variable "ec2_key_name" {
  description = "Key name for EC2"
  type        = string
  default     = "practicekey"
}

variable "ec2_userdata" {
  description = "bootstrap code"
  type = string
  default = "bootstrap.sh"
}

# Variables for VPC

variable "vpc_cidr_block" {
  description = "CIDR block for VPC"
  type        = string
  default     = "10.0.0.0/16"
}

variable "public_subnet_cidr_block_1" {
  description = "CIDR block for public subnet 1"
  type        = string
  default     = "10.0.0.0/18"
}

variable "public_subnet_availability_zone_1" {
  description = "Availability zone for public subnet 1"
  type        = string
  default     = "us-east-1a"
}

variable "public_subnet_cidr_block_2" {
  description = "CIDR block for public subnet 2"
  type        = string
  default     = "10.0.64.0/18"
}

variable "public_subnet_availability_zone_2" {
  description = "Availability zone for public subnet 2"
  type        = string
  default     = "us-east-1b"
}

variable "private_subnet_cidr_block_1" {
  description = "CIDR block for private subnet 1"
  type        = string
  default     = "10.0.128.0/18"
}

variable "private_subnet_availability_zone_1" {
  description = "Availability zone for private subnet 1"
  type        = string
  default     = "us-east-1a"
}

variable "private_subnet_cidr_block_2" {
  description = "CIDR block for private subnet 2"
  type        = string
  default     = "10.0.192.0/18"
}

variable "private_subnet_availability_zone_2" {
  description = "Availability zone for private subnet 2"
  type        = string
  default     = "us-east-1b"
}

variable "internet_gateway_name" {
  description = "Name for the internet gateway"
  type        = string
  default     = "dreamcatchers-igw"
}

variable "route_table_name" {
  description = "Name for the route table"
  type        = string
  default     = "dreamcatchers-rt"
}

variable "route_table_association_name_1" {
  description = "Name for route table association 1"
  type        = string
  default     = "dreamcatchers-rt-as-1"
}

variable "route_table_association_name_2" {
  description = "Name for route table association 2"
  type        = string
  default     = "dreamcatchers-rt-as-2"
}

variable "load_balancer_name" {
  description = "Name for the load balancer"
  type        = string
  default     = "dreamcatchers-lb"
}

variable "load_balancer_target_group_name" {
  description = "Name for the load balancer target group"
  type        = string
  default     = "dreamcatchers-lb-tg"
}

variable "load_balancer_listener_name" {
  description = "Name for the load balancer listener"
  type        = string
  default     = "dreamcatchers-lb-listener"
}

variable "target_group_name" {
  description = "Name for the target group"
  type        = string
  default     = "dreamcatchers-target-group"
}

variable "db_subnet_group_name" {
  description = "Name for the database subnet group"
  type        = string
  default     = "dreamcatchers-db-subnet-group"
}