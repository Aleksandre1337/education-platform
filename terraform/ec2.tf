# Public subnet EC2 instance 1
resource "aws_instance" "dreamcatchers-web-server-1" {
  ami             = var.ec2_ami
  instance_type   = var.ec2_instance_type
  security_groups = [aws_security_group.dreamcatchers-ec2-sg.id]
  subnet_id       = aws_subnet.dreamcatchers-pub-sub-1.id
  key_name   = var.ec2_key_name

  tags = {
    Name = "dreamcatchers-web-server-1"
  }

  user_data = file(var.ec2_userdata)
}

# Public subnet  EC2 instance 2
resource "aws_instance" "dreamcatchers-web-server-2" {
  ami             =  var.ec2_ami
  instance_type   = var.ec2_instance_type
  security_groups = [aws_security_group.dreamcatchers-ec2-sg.id]
  subnet_id       = aws_subnet.dreamcatchers-pub-sub-2.id
  key_name   = var.ec2_key_name

  tags = {
    Name = "dreamcatchers-web-server-2"
  }

    user_data = file(var.ec2_userdata)
}

#Elastic IP for EC2 instance 1 and 2

resource "aws_eip" "dreamcatchers-web-server-1-eip" {
  instance                  = aws_instance.dreamcatchers-web-server-1.id
  depends_on                = [aws_internet_gateway.dreamcatchers-igw]
}

resource "aws_eip" "dreamcatchers-web-server-2-eip" {
  instance                  = aws_instance.dreamcatchers-web-server-2.id
  depends_on                = [aws_internet_gateway.dreamcatchers-igw]
}