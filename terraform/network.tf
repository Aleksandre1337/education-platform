# VPC
resource "aws_vpc" "dreamcatchers-vpc" {
  cidr_block = var.vpc_cidr_block
  enable_dns_hostnames = true
  enable_dns_support   = true

  tags = {
    Name = "dreamcatchers-vpc"
  }
}

# Public Subnets
resource "aws_subnet" "dreamcatchers-pub-sub-1" {
  vpc_id            = aws_vpc.dreamcatchers-vpc.id
  cidr_block        = var.public_subnet_cidr_block_1
  availability_zone = var.public_subnet_availability_zone_1
  map_public_ip_on_launch = "true"

  tags = {
    Name = "dreamcatchers-pub-sub-1"
  }
}

resource "aws_subnet" "dreamcatchers-pub-sub-2" {
  vpc_id            = aws_vpc.dreamcatchers-vpc.id
  cidr_block        = var.public_subnet_cidr_block_2
  availability_zone = var.public_subnet_availability_zone_2
  map_public_ip_on_launch = "true"
  tags = {
    Name = "dreamcatchers-pub-sub-2"
  }
}

# Private Subnets
resource "aws_subnet" "dreamcatchers-pvt-sub-1" {
  vpc_id                  = aws_vpc.dreamcatchers-vpc.id
  cidr_block              = var.private_subnet_cidr_block_1
  availability_zone       = var.private_subnet_availability_zone_1
  map_public_ip_on_launch = false
  tags = {
    Name = "dreamcatchers-pvt-sub-1"
  }
}
resource "aws_subnet" "dreamcatchers-pvt-sub-2" {
  vpc_id                  = aws_vpc.dreamcatchers-vpc.id
  cidr_block              = var.private_subnet_cidr_block_2
  availability_zone       = var.private_subnet_availability_zone_2
  map_public_ip_on_launch = false
  tags = {
    Name = "dreamcatchers-pvt-sub-2"
  }
}

# Internet Gateway
resource "aws_internet_gateway" "dreamcatchers-igw" {
  tags = {
    Name = "dreamcatchers-igw"
  }
  vpc_id = aws_vpc.dreamcatchers-vpc.id
}

# Route Table
resource "aws_route_table" "dreamcatchers-rt" {
  tags = {
    Name = "dreamcatchers-rt"
  }
  vpc_id = aws_vpc.dreamcatchers-vpc.id
  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.dreamcatchers-igw.id
  }
}

# Route Table Association
resource "aws_route_table_association" "dreamcatchers-rt-as-1" {
  subnet_id      = aws_subnet.dreamcatchers-pub-sub-1.id
  route_table_id = aws_route_table.dreamcatchers-rt.id
}

resource "aws_route_table_association" "dreamcatchers-rt-as-2" {
  subnet_id      = aws_subnet.dreamcatchers-pub-sub-2.id
  route_table_id = aws_route_table.dreamcatchers-rt.id
}


# Create Load balancer
resource "aws_lb" "dreamcatchers-lb" {
  name               = "dreamcatchers-lb"
  internal           = false
  load_balancer_type = "application"
  security_groups    = [aws_security_group.dreamcatchers-alb-sg.id]
  subnets            = [aws_subnet.dreamcatchers-pub-sub-1.id, aws_subnet.dreamcatchers-pub-sub-2.id]

  tags = {
    Environment = "dreamcatchers-lb"
  }
}

resource "aws_lb_target_group" "dreamcatchers-lb-tg" {
  name     = "dreamcatchers-lb-tg"
  port     = 80
  protocol = "HTTP"
  vpc_id   = aws_vpc.dreamcatchers-vpc.id
}

# Create Load Balancer listener
resource "aws_lb_listener" "dreamcatchers-lb-listner" {
  load_balancer_arn = aws_lb.dreamcatchers-lb.arn
  port              = "80"
  protocol          = "HTTP"
  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.dreamcatchers-lb-tg.arn
  }
}

# Create Target group
resource "aws_lb_target_group" "dreamcatchers-loadb_target" {
  name       = "target"
  depends_on = [aws_vpc.dreamcatchers-vpc]
  port       = "80"
  protocol   = "HTTP"
  vpc_id     = aws_vpc.dreamcatchers-vpc.id

}

resource "aws_lb_target_group_attachment" "dreamcatchers-tg-attch-1" {
  target_group_arn = aws_lb_target_group.dreamcatchers-loadb_target.arn
  target_id        = aws_instance.dreamcatchers-web-server-1.id
  port             = 80
}
resource "aws_lb_target_group_attachment" "dreamcatchers-tg-attch-2" {
  target_group_arn = aws_lb_target_group.dreamcatchers-loadb_target.arn
  target_id        = aws_instance.dreamcatchers-web-server-2.id
  port             = 80
}

# Subnet group database
resource "aws_db_subnet_group" "dreamcatchers-db-sub" {
  name       = "dreamcatchers-db-sub"
  subnet_ids = [aws_subnet.dreamcatchers-pvt-sub-1.id, aws_subnet.dreamcatchers-pvt-sub-2.id]
}